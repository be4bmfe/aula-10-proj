package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }

    @Test
    public void testMult() throws Exception {
        Double result = calculator.execute("2*3");
        assertEquals("The mult result of 2 * 3 must be 6",  6D, (Object)result);
    }

    @Test
    public void testSub() throws Exception {
        Double result = calculator.execute("3-2");
        assertEquals("The sub result of 3 - 2 must be 1",  1D, (Object)result);
    }

    @Test
    public void testDiv() throws Exception {
        Double result = calculator.execute("4/2");

        assertEquals("The div result of 4 / 2 must be 2",  2D, (Object)result);
    }

    @Test
    public void testPotencia() throws Exception {
        Double result = calculator.execute("2^3");
        assertEquals("The potencia result of 2^3 must be 8",  8D, (Object)result);

        result = calculator.execute("2^0");
        assertEquals("The potencia result of 2^0 must be 1",  1D, (Object)result);

    }
    

}