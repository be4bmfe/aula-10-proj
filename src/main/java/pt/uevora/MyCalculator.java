package pt.uevora;

import java.util.Scanner;

public class MyCalculator {

    public Double execute(String expression){
       
        if(expression.contains("+")){
            String[] split = expression.split("\\+");
            return sum(split[0], split[1]);
        }

        if(expression.contains("*")){
             String[] split = expression.split("\\*");
            return mult(split[0], split[1]);
        }

        if(expression.contains("-")){
            String[] split = expression.split("\\-");
           return sub(split[0], split[1]);
        }

        if(expression.contains("/")){
            String[] split = expression.split("\\/");
            return div(split[0], split[1]);
        }
        if(expression.contains("^")){
            String[] split = expression.split("\\^");
            return potencia(split[0], split[1]);
        }

        throw new IllegalArgumentException("Invalid expression!");
    }
    private Double sum(String arg1, String arg2) {
        return new Double(arg1) + new Double(arg2);
    }

    private Double mult(String arg1, String arg2) {
        return new Double(arg1) * new Double(arg2);
    }

    private Double sub(String arg1, String arg2) {
        return new Double(arg1) - new Double(arg2);
    }

    private Double div(String arg1, String arg2) {
        return new Double(arg1) / new Double(arg2);
    }

    private Double potencia(String arg1, String arg2) {
        if(Integer.parseInt(arg2)==0){
            return new Double(1);
        }
        double result = new Double(arg1);
        for(int i=1;i<Integer.parseInt(arg2);i++){
            result=result * new Double(arg1);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("Calculator");
        System.out.println("Enter your expression:");
        Scanner scanner = new Scanner(System.in);
        String expression = scanner.nextLine();

        MyCalculator myCalculator = new MyCalculator();
        Object result = myCalculator.execute(expression);

        System.out.println("Result :  " + result);
    }

}
